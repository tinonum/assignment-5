#include "questionstructs.h"

questionstruct addQuestion(QString q, QString ans0,QString ans1,QString ans2,QString ans3,
                           bool ansbool0,bool ansbool1,bool ansbool2,bool ansbool3){

    struct questionstruct question1;
    question1.ques = q;
    question1.answers.push_back(ans0);
    question1.answers.push_back(ans1);
    question1.answers.push_back(ans2);
    question1.answers.push_back(ans3);
    question1.isRight.push_back(ansbool0);
    question1.isRight.push_back(ansbool1);
    question1.isRight.push_back(ansbool2);
    question1.isRight.push_back(ansbool3);

    return question1;
}
