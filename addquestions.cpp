#include "addquestions.h"

QVector<questionstruct> questionlistAdd(){
    QVector<questionstruct> questionlist;
    questionlist.push_back(addQuestion("What number is this: 1?", "Two", "One",
                                       "Three","Ten",false,true,false,false));
    questionlist.push_back(addQuestion("Who is the 1998 F1 world champion?", "Michael Schumacher", "Damon Hill",
                                       "Mika Häkkinen","Eddie Irvine",false,false,true,false));
    questionlist.push_back(addQuestion("When Finland won Olympic Gold in mens hockey?", "Lillehammer, 1994", "Salt Lake City, 2002",
                                       "Vancouver, 2010","Beijing, 2022",false,false,false,true));
    questionlist.push_back(addQuestion("How many kilometers are in a mile ?", "1.9 km", "2.2 km",
                                       "1.3 km","1.6 km",false,false,false,true));
    questionlist.push_back(addQuestion("How many lakes are in Finland?", "1,240", "11,286",
                                       "187,888","210,521",false,false,true,false));
    questionlist.push_back(addQuestion("Who is Aleksi 'allu' Jalli?", "Pro CS:GO player", "Actor",
                                       "News Reported","Famous Finnish Chef",true,false,false,false));
    questionlist.push_back(addQuestion("How many Breaking Bad seasons are there?", "Four", "Six",
                                       "Five","Seven",false,false,true,false));
    questionlist.push_back(addQuestion("How many saunas are in Finland approximately?", "3.3 million", "2.9 million",
                                       "3.5 million","2.3 million",true,false,false,false));
    questionlist.push_back(addQuestion("Which city is believed to be the oldest?", "Jericho, Plestinian Territories", "Damascus, Syria",
                                       "Athens, Greece","Jerusalem, Israel",true,false,false,false));
    questionlist.push_back(addQuestion("Who created this Quiz?", "Juho and Tino", "Juho and Tino",
                                       "Juho and Tino","Juho and Tino",true,true,true,true));

    return questionlist;
}

