#include "question.h"

Question::Question(QVector<questionstruct> listIn, QObject *parent)
    : QObject{parent}
{
    this->setList(listIn);
    this->timer = new QTimer(this);
    connect(timer,&QTimer::timeout,
            this,&Question::timeoutHappened);
    timer->start(20000);

    this->updater = new QTimer(this);
    connect(updater,&QTimer::timeout,
            this,&Question::sendTimer);
    updater->start(500);

}
void Question::sendTimer(){
    int timeleft = this->timer->remainingTime();
    emit updateTimer(timeleft);
//    qInfo()<<timeleft;
}


void Question::setList(QVector<questionstruct> listIn){
    this->questionList = listIn;
    this->questionAmount = listIn.size();
}

void Question::initializeQuestion(){
    emit updateQuestion(this->questionList[turnCount]);

}

void Question::timeoutHappened(){
    turnCountIncrement();
    emit updateQuestion(this->questionList[turnCount]);
    qInfo()<<"Timeout happened";

}

void Question::pressed1(){
    qInfo()<<"button 1 was pressed";
    if(this->questionList[turnCount].isRight[0] == true){
        qInfo()<<"answer was correct";
        this->promptCorrect();
    }else{
        qInfo()<<"answer was wrong";
        this->promptWrong();
    }
    turnCountIncrement();
    emit updateQuestion(this->questionList[turnCount]);
}

void Question::pressed2(){
    qInfo()<<"button 2 was pressed";
    if(this->questionList[turnCount].isRight[1] == true){
        qInfo()<<"answer was correct";
        this->promptCorrect();
    }else{
        qInfo()<<"answer was wrong";
        this->promptWrong();
    }
    turnCountIncrement();
    emit updateQuestion(this->questionList[turnCount]);
}

void Question::pressed3(){
    qInfo()<<"button 3 was pressed";
    if(this->questionList[turnCount].isRight[2] == true){
        qInfo()<<"answer was correct";
        this->promptCorrect();
    }else{
        qInfo()<<"answer was wrong";
        this->promptWrong();
    }
    turnCountIncrement();
    emit updateQuestion(this->questionList[turnCount]);
}

void Question::pressed4(){
    qInfo()<<"button 4 was pressed";
    if(this->questionList[turnCount].isRight[3] == true){
        qInfo()<<"answer was correct";
        this->promptCorrect();
    }else{
        qInfo()<<"answer was wrong";
        this->promptWrong();
    }
    turnCountIncrement();
    emit updateQuestion(this->questionList[turnCount]);
}

void Question::turnCountIncrement(){
    this->turnCount++;
    qInfo()<<"turncount is "<<this->turnCount;
    if(this->turnCount == this->questionAmount){
        turnCount = 0;
    }
    this->timer->start();
}

void Question::promptCorrect(){
    this->calculatePoints();
    QMessageBox msg;
    msg.setWindowTitle(" ");
    msg.setText("Correct!");
    msg.exec();

}

void Question::promptWrong(){
    QMessageBox msg;
    msg.setWindowTitle(" ");
    msg.setText("Wrong!");
    msg.exec();

}

void Question::calculatePoints(){
    int points = this->timer->remainingTime();
    points = points / 20 ;
    emit this->sendPoints(points);
    emit this->updatePoints(this->player->getPoints());
    qInfo()<<"total "<<this->player->getPoints();
    qInfo()<<"added "<<QString::number(points);

}

void Question::setPlayer(Player *player){
    this->player = player;
    qInfo()<<this->player->nameget();
}

