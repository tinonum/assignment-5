#include "player.h"

Player::Player(QObject *parent)
    : QObject{parent}
{

}

void Player::getName(QString name)
{
    this->player_name=name;
}

int Player::getPoints(){
    return this->points;
}

void Player::setPoints(int value){
    this->points = this->points + value;
}

QString Player::nameget(){
    return this->player_name;
}
