#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "question.h"
#include "player.h"
#include <QMainWindow>
#include <QInputDialog>
#include <QDir>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Question *question, Player *player,QWidget *parent = nullptr);
    void formConnections(Question *questionIn, Player *playerIn);
    void userName();

    ~MainWindow();

private:
    Ui::MainWindow *ui;

signals:
    void sendName(QString name);


public slots:
    void updateTexts(struct questionstruct question1);
    void updateTimer(int timeRemaining);
    void updatePoints(int points);
};
#endif // MAINWINDOW_H
