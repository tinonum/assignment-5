## NAME
Qt: Assignment 6

## DESCRIPTION
Multiple choice quiz game

## PROGRAM FAQ
Program starts by asking user for username. 

Then User has 20 seconds to answer the question and then user get's rewarded by points based on how fast user responded.

Questions have multiple choices of possible answers and only one of them is correct.

Based on if answer is correct or not. Program will have a pop up window with either 'Correct!' or 'Wrong!'


Screenshot of the GUI.

![gui](./gui_screenshot.jpg)

UML Diagram

![uml](./UML_diagram.jpg)

## AUTHORS
Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Juho Kangas | [@jhkangas3](https://gitlab.com/jhkangas3)
