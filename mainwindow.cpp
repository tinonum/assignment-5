#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(Question *question, Player *player, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // initialize necessary connections between question and main window ui elements
    this->formConnections(question, player);
    // initialize questions on main window
    question->initializeQuestion();

    this->userName();

}

void MainWindow::formConnections(Question *questionIn, Player *playerIn){
    connect(this,&MainWindow::sendName, playerIn, &Player::getName);
    connect(ui->pushButton_1,SIGNAL(clicked()),questionIn,SLOT(pressed1()));
    connect(ui->pushButton_2,SIGNAL(clicked()),questionIn,SLOT(pressed2()));
    connect(ui->pushButton_3,SIGNAL(clicked()),questionIn,SLOT(pressed3()));
    connect(ui->pushButton_4,SIGNAL(clicked()),questionIn,SLOT(pressed4()));
    connect(questionIn,&Question::updateQuestion,
            this,&MainWindow::updateTexts);
    connect(questionIn,&Question::updateTimer,
            this,&MainWindow::updateTimer);
    connect(questionIn, &Question::sendPoints, playerIn, &Player::setPoints);
    connect(questionIn, &Question::updatePoints, this, &MainWindow::updatePoints);
}
void MainWindow::updateTexts(struct questionstruct question1){
    this->ui->pushButton_1->setText(question1.answers[0]);
    this->ui->pushButton_2->setText(question1.answers[1]);
    this->ui->pushButton_3->setText(question1.answers[2]);
    this->ui->pushButton_4->setText(question1.answers[3]);
    this->ui->label_question->setText(question1.ques);
}

void MainWindow::updatePoints(int points){
    this->ui->label_points->setText(QString::number(points));
}

void MainWindow::updateTimer(int timeRemaining){
    this->ui->label_timer->setText(QString::number(timeRemaining / 1000));
}

void MainWindow::userName()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Your name"), tr("Add username"), QLineEdit::Normal, QDir::home().dirName() , &ok);
    if (ok && !name.isEmpty())
    {
        ui->user_name_label->setText(name);
        emit sendName(name);
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

