#ifndef QUESTIONSTRUCTS_H
#define QUESTIONSTRUCTS_H

#include <QVector>
#include <QString>

struct questionstruct{
    QString ques;
    QVector<QString> answers;
    QVector<bool> isRight;
};

questionstruct addQuestion(QString q, QString ans0,QString ans1,QString ans2,QString ans3,
                           bool ansbool0,bool ansbool1,bool ansbool2,bool ansbool3);


#endif // QUESTIONSTRUCTS_H
