#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QDebug>
#include <QInputDialog>
#include <QDir>

class Player : public QObject
{
    Q_OBJECT

private:
    QString player_name;
    int points;

public:
    explicit Player(QObject *parent = nullptr);
    int getPoints();
    QString nameget();

signals:

public slots:
    void getName(QString name);
    void setPoints(int value);


};

#endif // PLAYER_H
