#ifndef QUESTION_H
#define QUESTION_H

#include <QObject>
#include <QVector>
#include <QString>
#include <QtDebug>
#include "questionstructs.h"
#include <QTimer>
#include <QMessageBox>
#include <player.h>



class Question : public QObject
{
    Q_OBJECT

private:
    QVector<questionstruct> questionList;
    int turnCount = 0;
    int questionAmount = 0;
    Player *player;

public:
    explicit Question(QVector<questionstruct> listIn, QObject *parent = nullptr);
    void setList(QVector<questionstruct>);
    void turnCountIncrement();
    void initializeQuestion();
    QTimer *timer;
    QTimer *updater;
    void promptCorrect();
    void promptWrong();
    void calculatePoints();
    void setPlayer(Player *player);


signals:
    void updatePoints(int points);
    void updateQuestion(questionstruct questionOut);
    void updateTimer(int time);
    void sendPoints(int value);

public slots:
    void pressed1();
    void pressed2();
    void pressed3();
    void pressed4();
    void timeoutHappened();
    void sendTimer();


};

#endif // QUESTION_H
